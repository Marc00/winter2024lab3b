public class Dolphin{
	public double length;
	public int weight;
	public int swimSpeed;	
	
	public void ballTrick(){
		System.out.println("The dolphin balances a ball at the tip of its mouth at a height of " + this.length/2);
	}
	
	public void jump(){
		System.out.println("The dolphin jumps " + this.weight / this.swimSpeed + " meters high");
	}
}