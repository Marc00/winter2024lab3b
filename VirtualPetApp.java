import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String args[]){
		Scanner reader = new Scanner(System.in);
		
		Dolphin[] podOfDolphin = new Dolphin[4];
		
		for(int i = 0; i < podOfDolphin.length; i++){
			podOfDolphin[i] = new Dolphin();
			System.out.println("Imput a length for the dolphin number " + (i + 1) + " (whole number)");
			podOfDolphin[i].length = Integer.parseInt(reader.nextLine());
			System.out.println("Imput a weight for the dolphin number " + (i + 1) + " (whole number)");
			podOfDolphin[i].weight = Integer.parseInt(reader.nextLine());
			System.out.println("Imput a swim speed for the dolphin number " + (i + 1) + " (whole number)");
			podOfDolphin[i].swimSpeed = Integer.parseInt(reader.nextLine());
			
		}
		
		System.out.println("Length of dolphin " + podOfDolphin.length + ": " + podOfDolphin[podOfDolphin.length - 1].length);
		System.out.println("Weight of dolphin " + podOfDolphin.length + ": " + podOfDolphin[podOfDolphin.length - 1].weight);
		System.out.println("Swim speed of dolphin " + podOfDolphin.length + ": " + podOfDolphin[podOfDolphin.length - 1].swimSpeed);
		
		podOfDolphin[0].ballTrick();
		podOfDolphin[0].jump();		
	}
}